import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        name:'home',
        path: '/',
        component: () => import('@/views/HomePage')
    },
    {
        name:'options',
        path: '/options',
        component: () => import('@/views/OptionsPage')
    },
    {
        name:'question',
        path: '/questions',
        component: () => import('@/views/QuestionPage')
    },
    {
        name:'result',
        path:'/results',
        component: () => import('@/views/ResultsPage')
    }
];


export default createRouter({
    history: createWebHistory(),
    routes
});