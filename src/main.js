import {createApp} from 'vue';
import App from './App.vue';
import './assets/styles/style.css';
import router from './config/routerConfig';
import store from '@/store/store';

createApp(App)
    .use(router)
    .use(store)
    .mount('#app');

