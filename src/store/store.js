import {createStore} from 'vuex';

const store = createStore({
    state: {
        user: {id: '', username: '', highScore: 0},
        categories:{trivia_categories:[]},
        options: {amount: 0, category: '', difficulty: '', type:''},
        questions: [],
        userAnswers: []
    },
    getters: {
        getUser: (state) => {return state.user},
        getCategories: (state) => {return state.categories.trivia_categories},
        getQuestions: (state) => {return state.questions},
        getOptions: (state) => {return state.options},
        getUserAnswers: (state) => {return state.userAnswers}
    },
    mutations: {
        setUser: (state,payload) =>{
            state.user = payload
        },
        setCategories: (state, payload) => {
            state.categories.trivia_categories = payload;
        },
        setQuestions: (state, payload) => {
            state.questions = payload;
        },
        setOptions: (state, payload) => {
            state.options = payload;
        },
        setUserAnswers: (state, payload) => {
            state.userAnswers = payload;
        },
        setHighScore: (state, payload) => {
            state.user.highScore = payload;
        }
    },

    actions: {}
});

export default store;