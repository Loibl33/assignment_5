const host = 'https://opentdb.com';

async function getAllCategories() {
    try {
        const response = await fetch(host + '/api_category.php', {method: 'GET'});

        if (!response.ok) {
            throw new Error('Fetching data not successful');
        }
        return response.json();
    } catch (e) {
        alert(e.message);
    }
}

async function getQuestions(amount, categoryId, difficulty, type) {
    try {
        const response = await fetch(host +
            `/api.php?amount=${amount}&category=${categoryId}&difficulty=${difficulty}&type=${type}&encode=base64`,
            {method: 'GET'});

        if (!response.ok) {
            throw new Error('Fetching data not successful');
        }

        return response.json();
    } catch (e) {
        alert(e.message);
    }
}


export {getAllCategories, getQuestions};