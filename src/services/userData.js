const host = 'https://assignment5-vue-dimitrov-loibl.herokuapp.com';
const apiKey = 'sdxtnepfrhgasecbtlaonljesdyplvwdziuygrfulbburyyevwousprvalhdmmrg';
async function getUserData(username) {
    try {
        const response = await fetch(host + `/trivia?username=${username}`, {method: 'GET'});
        if(!response.ok){
            throw new Error('Fetching data not successful');
        }
        return response.json();
    } catch (e) {
        alert( + e.message);
    }
}

async function createUser(user) {
    try {
        const response = await fetch(host + `/trivia`,
            {
                method: 'POST',
                headers: {
                    'X-API-Key': apiKey,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            });
        if(!response.ok){
            throw new Error('Creating not successful');
        }
        return response.json();
    }catch(e){
        alert( e.message);
    }
}

async function updateUserScore(id,highScore) {
    try {
        const response = await fetch(host + `/trivia/${id}`,
            {
                method: 'PATCH',
                headers: {
                    'X-API-Key': apiKey,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({highScore: highScore})
            });

        if(!response.ok){
            throw new Error('Updating not successful');
        }

        return response.json();
    }catch(e){
        alert( e.message);
    }
}

export {getUserData, createUser, updateUserScore};