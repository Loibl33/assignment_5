# Assignment-5: Trivia Game

This application implements a trivia game using Vue.js

## Storage
To store the data we use a json server deployed at https://assignment5-vue-dimitrov-loibl.herokuapp.com/

## Usage

The application is deployed at: https://ancient-lowlands-53028.herokuapp.com

If you want to run it yourself, you simpy need to clone the repo and run</br> ```npm install; npm run serve```

## UI

Here you can see how the page looks 
<details>
  <summary>home page</summary>
  <img src="screenshots/HomePage.jpg">
</details>
<details>
  <summary>options page</summary>
  <img src="screenshots/OptionsPage.jpg">
</details>
<details>
  <summary>question page</summary>
  <img src="screenshots/QuestionPage.jpg">
</details>
<details>
  <summary>results page</summary>
  <img src="screenshots/ResultsPage.jpg">
</details>


The mockups can be found online <a href="https://www.figma.com/file/9aVTC0wrmwWavBfEQIe15A/Trivia-app?node-id=0%3A1">here</a> and <a href="trivia-app.pdf" type=file>here</a> as pdf document

## Maintainer

[Petar Dimitrov]

[Philipp Loibl]

## License

[MIT]
---

[Petar Dimitrov]: https://github.com/PetarDimitrov91

[Philipp Loibl]: https://github.com/Loibl33

[MIT]: https://choosealicense.com/licenses/mit/


